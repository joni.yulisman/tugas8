<?php

Route::get('/', 'IndexController@index') ; 
Route::get('/register', 'AuthController@index');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/master', function() {
    return view('layout.master');
});

Route::get('/data-table', function() {
     return view('table.data-table');
});
  
//crud cast
//create
Route::get('/cast/create', 'CastController@create'); //create
Route::post('/cast', 'CastController@store');

//read
Route::get('/cast/', 'CastController@index'); //read
Route::get('/cast/{cast_id}', 'CastController@show'); //read detail

//edit
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //edit detail
Route::put('/cast/{cast_id}', 'CastController@update'); //read detail

//del
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //read detail

