@extends('layout.master')
@section('judul') <h1>Buat Account Baru</h1> @endsection
@section('content')

<form action="/welcome" method="POST">
    @csrf
    <label for="fname">Firstname :</label><br>
    <input type="text" name="namaDepan" id="firstname"><br><br>
    <label for="fname">Lastname :</label><br>
    <input type="text" name="namaBelakang" id="lastname"><br><br>
    <label for="fname">Gender :</label><br>
    <input type="radio" name="gender" id="male" value="male"> Male
    <input type="radio" name="gender" id="female" value="female"> Female <br><br>
    <label for="fname">Nasionality :</label><br>
    <select name="nasionality" id="nasionality">
        <option value="indonesia">Indonesia</option>
        <option value="malaysia">Malaysia</option>
        <option value="brunei">Brunei</option>
    </select><br><br>
    <label for="fname">Languange Spoken :</label><br>
    <input type="checkbox" name="languange" id="indo" value="indonesia">Indonesia <br>
    <input type="checkbox" name="languange" id="english" value="english">English <br>
    <input type="checkbox" name="languange" id="arab" value="arab">Arab <br> <br>
    <label for="Biodata">Biodata</label><br>
    <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
    <input type="submit" value="kirim">
</form>

@endsection