@extends('layout.master')
@section('judul')
<h1>List Cast</h1>
@endsection
@section('content')

<a href="http:/cast/create" class="btn btn-primary mb-3">Tambah Pemeran</a>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Umur</th>
            <th scope="col">Bio</th>
            <th scope="col">Action</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1 }}</td>
            <td>{{$item ->nama}}</td>
            <td>{{$item ->umur}}</td>
            <td>{{$item ->bio}}</td>
            <td><a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a></td>
            <td><a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a></td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
        @empty
        <h1>Data Tidak Ada</h1>
        @endforelse
    </tbody>
</table>




@endsection