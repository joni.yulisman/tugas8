@extends('layout.master')
@section('judul')
List Cast
@endsection
@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('put')
    <div class="form-group">
        <label>Nama :</label>
        <input type="text" name="nama" id="nama" value="{{$cast->nama}}" class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Umur :</label>
        <input type="text" name="umur" id="umur" value="{{$cast->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <div class="form-group">
        <label>Biodata :</label>
        <textarea name="bio" id="bio" cols="30" rows="10" class="form-control">{{$cast->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">submit</button>
</form>

@endsection